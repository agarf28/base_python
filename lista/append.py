import time

print("APPEND SIMPLES")
valores = []
valores.append(5)
valores.append(9)
valores.append(4)
print(valores)
print("")
time.sleep(5)

print("VALORES FORMATADOS")
for v in valores:
    print(f'{v}...')
print("")
time.sleep(5)

print("VALORES FORMATADOS NA MESMA LINHA")
for v in valores:
    print(f'{v}...', end='')
print("")
time.sleep(5)

print("CHAVES E VALORES")
for c, v in enumerate(valores):
    print(f'Na posição {c} encontrei o valor {v}!')
print('Cheguei ao final da lista')
print('')
time.sleep(5)

print('LER VALORES PELO TECLADO')
print('E COLOCAR NA LISTA')
valores = list()
for cont in range(0, 5):
    valores.append(int(input('Digite um valor:   ')))

for c, v in enumerate(valores):
    print(f'Na posição {c} encontrei o valor {v}!')
print('Cheguei ao final da lista.')
print("")
time.sleep(5)

print("PECULIARIEDADE - LISTA B == LISTA A")
a = [2, 3, 4, 7]
b = a

print(f'Lista A: {a}')
print(f'Lista B: {b}')
print('')
time.sleep(5)

print("COLOCANDO UM VALOR DIFERENTE")
print("PARA B NA POSIÇÃO 2")
b[2] = 8
print(f'Lista A: {a}')
print(f'Lista B: {b}')
print("")
time.sleep(5)

print('B CRIA UMA COPIA DE A')
b = a[:]
print(f'Lista A: {a}')
print(f'Lista B: {b}')
print("")
time.sleep(5)

print("COLOCANDO UM VALOR DIFERENTE")
print("para B na posiçao 2")
a = a[:]
b[2] = 90
print(f'Lista A: {a}')
print(f'Lista B: {b}')
print("")
time.sleep(6)

print('TERMINEI O PROGRAMA')
