import time
times = ('Corintians', 'Palmeiras', 'Santos', 'Grêmio',
         'Cruzeiro', 'Flamengo', 'Vasco', 'Chapecoense',
         'Atletico', 'Botafogo', 'Atletico-PR', 'Bahia',
         'Sao Paulo', 'Fluminense', 'Sport Recife', 
         'EC Vitoria', 'Coritiba', 'Avai', 'Ponte Preta',
         'Atletico-GO')

print('-=-' * 15)
print(f'Lista de times: {times}')
time.sleep(5)
print('-=-' * 15)
print(f'Os 5 primeiros são {times[0:5]}')
time.sleep(5)
print('-=-' * 15)
print(f'Os últimos 4 colocados da tabela são {times[-4:]}')
time.sleep(5)
print('-=-' * 15)
print(f'Times em Ordem Alfabética: {sorted(times)}')
print('-=-' *15)
time.sleep(5)
print(f'O Chapecoense está na {times.index("Chapecoense")+1} posição')




