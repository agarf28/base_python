import time
"""
Crie uma tupla preenchida com os 20 colocados da
tabela do campeonato brasileiro de Futebol, na 
ordem de colocação. Depois mostre:
A - apenas os cinco primeiros colocados.
B - Os últimos quatros colocados da tabela.
C - a lista com os times em ordem alfabetica.
D - Em que posição da tabela está o time da chapecoense.
"""

times_tabela = ('flamengo', 'santos', 'palmeiras', 'gremio',
                'atletico-PR', 'são paulo', 'internacional',
                'corinthians', 'fortaleza', 'goias', 'bahia',
                'vasco', 'atletico-MG', 'fluminense',
                'botafogo', 'ceara', 'cruzeiro',
                'csa', 'chapecoense', 'avai')


print('-=-' * 20)
print('OS CINCO PRIMEIROS COLOCADOS:')
print(times_tabela[0:5])
time.sleep(5)
print('-=-' * 20)
print('OS ÚLTIMOS QUATRO COLOCADOS TA TABELA')
print(times_tabela[-5:-1])
time.sleep(5)
print('-=-' * 20)
print('TIMES EM ORDEM ALFABETICA')
print(sorted(times_tabela))
time.sleep(5)
print('-=-' * 20)
print('POSIÇAO DA CHAPECOENSE NA TABELA')
print(f'A Chapecoense está na posiçao {times_tabela[chapecoense]}')
