lanche = ('Hamburguer', 'Suco', 'Pizza', 'Batata Frita')
print("feito com range")
for cont in range(0, len(lanche)):
    print(f'Eu vou comer {lanche[cont]}')
print("")

print("Sem range")
for comida in lanche:
    print(f'Eu vou comer {comida}')
print("")
print("Com enumerate")
print("")
for pos, comida in enumerate(lanche):
    print(f'Eu vou comer {comida} na posição {pos}')

print('Comi pra caramba!')
