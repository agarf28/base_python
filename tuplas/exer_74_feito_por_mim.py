"""
 HEAD
Crie um programa que vai gerar cinco
números aleatórios e colocar em uma
tupla.
Depois disso, mostre a listagem de
números gerados e também indique o
menor e o maior valor que estão
na tupla.
"""
print("=======")



import random

cinco_numeros_aleatorios = random.randint()

for i in range(5):
    cinco_numeros_aleatorios.append(random.randint())

cinco_numeros_aleatorios = tuple(cinco_numeros_aleatorios)
print(cinco_numeros_aleatorios)
